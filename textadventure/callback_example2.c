#include <stdio.h>

// calling function
void process(char *(*whichmethod)(void)) {
    printf("%s\n", whichmethod());
    }

char *look(void) {
  return "You don't see anything\n";
}

char *go(void) {
  return "Where do you want to go?\n";
}

int main(void) {
  process(&look);
  process(&go);

  return 0;
}

