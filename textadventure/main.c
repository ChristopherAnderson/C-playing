#include <stdio.h>
#include <stdbool.h>
#include "parse_exec.h"
#include "input.h"
#include "init.h"
#include "locs.h"

int main(void) {

 static char input[100] = "look around";
  intro();
  // alternately calls parse_exec() and getInput()
  while (parse_exec(input) && getInput(input));
  printf("\nBye\n");
  
  printf("%i\n", numberOfLocs);
  return 0;
}
