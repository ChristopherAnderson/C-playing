#pragma once

struct location {
    const char *description;
    const char *tag;
};

struct location locs[] = {
    {"under the sycamore", "tree"},
    {"standing in the road", "road"}
};

#define numberOfLocs (sizeof(locs) / sizeof(*locs))