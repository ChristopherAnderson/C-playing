#include <stdio.h>
#include <stdbool.h>
#include "input.h"

bool getInput(char *in) {
  
  printf("\n--> ");
  return fgets(in, sizeof(&in), stdin) != NULL;
}

