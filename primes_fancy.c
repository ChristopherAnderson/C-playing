#include <math.h>
#include <stdio.h>
#include <signal.h>
#include <malloc.h>

int ct=0, keepgoing=1;
int *primes = NULL;

void breakhere() { keepgoing=0; }

int main(void) {
  int i, testme=2, isprime;

  signal(SIGINT, breakhere);
  while(keepgoing) {
    isprime = 1;
    // fails when isprime=0, no test past sqrt, run out f prev primes to test
    for (i=0; isprime && i<sqrt(testme) && i<ct; i++)
    { isprime = testme % primes[i]; } // 0 when factor
    if (isprime) {
      printf("%i \r", testme); fflush(NULL);
      primes = realloc(primes, sizeof(int)*(ct+1));
      primes[ct] = testme;
      ct++;
    }
    testme++; // next prime test
  }
  printf("\n");
  for (i=0; i<ct; i++)
  { printf("%i\t", primes[i]); }
  printf("\n");
}

