// bday problem organized into functions
#include <math.h>
#include <stdio.h>

typedef struct {
  double one_match;  // probability someone matches 1st person's bday
  double none_match; // probably that no two people share a bday
} bday_struct;

int upto=40;

// see first bday prob for explanation of calculation
void calculate_days(bday_struct days[]) {
  int count;
  days[1].none_match = 1;
  for (count=2; count<=upto; count++) {
    days[count].one_match = 1 - pow(364/365., count-1); 
    days[count].none_match = days[count-1].none_match * (1-(count-1)/365.);
  }
}
void print_days(bday_struct days[]) {
  int count;
  printf("People\t Matches me\t Any match\n");
  for(count=2; count<=upto; count++) {
    printf("%i\t %.3f\t\t %.3f\n", 
            count, days[count].one_match, 1-days[count].none_match);
  }
}
int main() {
  int ct, upto = 40;
  bday_struct days[upto+1]; // an array of bday_structs
  calculate_days(days);
  print_days(days);

  return 0;
}











