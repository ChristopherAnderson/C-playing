// declaration, promises such a thing exists
// since the definition is below main(), without this declaration we Error
double calculation(int, int);  

int main()
{
  // counter can be defined because it has been declared here
  int a_var, counter=0;

  char aLetter(char ok)
  {   // can be defined because it has been declared here
    return ok;
  }
  calculation(5,5);

  return 0;
}

double calculation(int a, int b)
{
  return a*b;
}
