#include <stdio.h>
#include <stdlib.h>

int main()
{
  int i = 0;
  while (i < 5)
  {
    printf("i is %d\n", i);
    ++i;
  }
  printf("hmmmm, i is %d\n", i);
  while (i < 50)
  {
    printf("i is %d, i*5 is %d\n", i, i*5);
    i *= 5;
  }
  for (i = 1; 12%i != 3; ++i)
  {
    printf("hello, i is %d\n", i);
  }

  int num = rand();  // not as expected, oh well
  do {
    printf("random: %d\n", num);
    num = rand() % 100;
  } while (num < 70);

  return 0;
}

