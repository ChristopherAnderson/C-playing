/* & (bitwise AND): the result is 1 if both are 1
 * | (bitwise OR): the result is 1 if either are 1
 * ^ (bitwise XOR): the result is 1 if they are different
 * << (leftshift): b<<2 shifts bits of b 2 places left
 * >> (rightshift):    same, to the right
 * ~ (bitwise NOT): inverts the bits
 * */

#include <stdio.h>

int max(int x,int y) {
  int max;
  // 3 XOR ((3 XOR 6) AND -(3 < 6))
  // 0011 XOR ((0011 XOR 0110) AND -(0011 < 0110))
  // 0011 XOR (    0101        AND -1)
  // 0011 XOR     0101 & (-1) == 0101  // why though
  // 0011 XOR 0101
  // 0110 = 6
  //               If reversed, -(6<3) would be 0.
  //               Then 0011 AND 0000 is 0,
  //               0101 XOR 0000 = 0101 = 6
  max = x ^ ((x ^ y) & -(x < y));

  return max;
}

int main(void) {
  printf("max(3,6): %i, max(6,3): %i\n", max(3,6), max(6,3));
  printf("%li\n", sizeof(char));
  for (int i=0; i<16; i++) {
    printf("%i & -1 is: %d\n", i, i & (-1));
  }
  return 0;
}
