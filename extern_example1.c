// explicit definition
// allocates as well as describes
int Global_var;

// function prototype (declaration)
// assumes definition elsewhere
void SomeFunction(void);

int main(void)
{
  Global_var = 1;
  SomeFunction();
  return 0;
}
