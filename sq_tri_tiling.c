#include <stdio.h>
#include <stdlib.h>

typedef struct square {
  int index;
  struct triangle *tri;
  struct square *left;
  struct square *right;
} square;

typedef struct triangle {
  int index;
  struct square *sq;
  struct triangle *left;
  struct triangle *right;
} triangle;

square *create_sq(int index, square *left, square *right, triangle *tri) {
  square *new_square = (square*)malloc(sizeof(square));
  if (new_square == NULL) {
    printf("Error creating new square\n");
    exit(1);
  }
  new_square->index = index;
  new_square->tri = tri;
  new_square->left = left;
  new_square->right = right;

  return new_square;
}

triangle *create_tri(int index, triangle *left, triangle *right, square *sq) {
  triangle *new_triangle = (triangle*)malloc(sizeof(triangle));
  if (new_triangle == NULL) {
    printf("Error creating new triangle\n");
    exit(1);
  }
  new_triangle->index = index;
  new_triangle->sq = sq;
  new_triangle->left = left;
  new_triangle->right = right;

  return new_triangle;
}

int main(void) {

  return 0;
}
