#include <stdio.h>
#include <malloc.h>
#include "gamehelpers.h"

char statusbar[] = "Player: %s | points: %i | level %i | %s: exits %s";
void status(void) {// take arguments later
  printf(statusbar, "helmar", 13, 21, "Room1", "NSE");
  printf("\n");
}

player addplayer(int *playercount, int **players) {
  player newplayer = {.id = (1 << *playercount) };;
  *players = realloc(*players, sizeof(int)*(*playercount+1));
  (*players)[*playercount] = newplayer.id;
  (*playercount)++;
  
  printf("Added player id: %i\n", newplayer.id);
  return newplayer;
}

void openclose(room *r, int direction) {
  r->exits ^= direction;
}

// trick for counting set bits from Kernighan 
int exitcount(int exits) {
  int count=0;
  while(exits) {
    exits = exits & (exits-1);
    count++;
  }
  return count;
}