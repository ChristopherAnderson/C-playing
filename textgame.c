#include <stdio.h>
#include <malloc.h>
#include "gamehelpers.h"
#define N 1
#define E 2
#define S 4
#define W 8


int main(void) {
  int playercount=0;
  int *players = NULL;
  room room1 = {.exits = N | S };
  room room2 = {.exits = E | S | W };
  room *currentroom = &room1;

  status();
  // toggle doors
  printf("room1 exits: %i\n", room1.exits);
  openclose(&room1, E);
  printf("toggled E, room1 exits: %i\n", room1.exits);
  openclose(&room1, E);
  printf("toggled E, room1 exits: %i\n", room1.exits);

  player frank = addplayer(&playercount, &players);
  printf("playercount: %i\n", playercount);
  player helmar = addplayer(&playercount, &players);
  printf("playercount: %i\n", playercount);
  player meltor = addplayer(&playercount, &players);
  printf("playercount: %i\n", playercount);
  player kangfz = addplayer(&playercount, &players);
  printf("playercount: %i\n", playercount);
  printf("kangfz's id: %i\n", kangfz.id);

  printf("room1 has %i exits, room2 has %i exits\n", 
          exitcount(room1.exits), exitcount(room2.exits));
  return 0;
}
