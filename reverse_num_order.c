#include <stdio.h>
// from SO, interesting thread
// https://stackoverflow.com/questions/1068849/how-do-i-determine-the-number-of-digits-of-an-integer-in-c
// incorporate into loop though
int countDigits(int value)
{
  int result = 0;
  while( value != 0 ) 
  {
    value /= 10;
    result++;
  }
  return result;
}

int main()
{
  int val = 12345;
  int digits = countDigits(val);
//  for (int i = 0; i!=5; ++i)
//  {
//    val = (val / 10) % 10;
//    printf("%g\n", val);
//  }

  printf("%d\n", (val/(int)10e0) % 10);
  printf("%d\n", (val/(int)10e1) % 10);
  printf("%d\n", (val/(int)10e2) % 10);
  return 0;
}
