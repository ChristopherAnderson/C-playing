/* 
 *  runtime checks to all risky pointer operations to 
 *  catch UB. This effectively immunizes your program again buffer 
 *  overflows and helps to catch all kinds of dangling pointers.
 *  
 *  does not catch at compile, only at runtime
 *
 *  use:
 *  gcc -fsanitize-address sanitize-address.c
 */

int main()
{
  int a[10];
  a[10]=1;   // oh no!
}

