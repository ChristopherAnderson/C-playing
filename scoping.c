#include <stdio.h>

int global = 1;

void whatsit(char chr_copy)
{
  chr_copy = 'n';
  printf("chr got changed to: %c in whatsit\n", chr_copy);
//  printf("%i\n", a); // a is not in the frame
  printf("global was defined outside everything, it's: %i\n", global);
}

int main()
{
  int a = 2;
  char chr = 'x';
  printf("before whatsit, chr is: %c\n", chr);
  whatsit(chr);     // a not in frame, global is though
                    // a copy of chr is in the frame, and is
                    // destroyed when the function call completes
                    // the value is changed in the function but when the
                    // frame is destroyed it's back
  printf("outside whatsit, chr is back to: %c\n", chr);                    


  // definition inside of main()
  // never a reason to do this I can think of, just demonstrating
  void something(void)
  {
    printf("a was defined in main(), so was this function. a is: %i\n", a); // a is "global" to the definition
    global = 45;
    printf("switched global to %i in something()\n", global);
  }
  something();    // prints "2"

  printf("before we exit main, global has changed! to: %i\n", global);
  return 0;
}
