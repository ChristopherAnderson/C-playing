void print_array(int in_array[], int array_size);
void swap(int *a, int *b);
void bitswap(int *a, int *b);
int bitmax(int x, int y);
// binarystring
int digitcount(int N);
int bitabs(int x);
_Bool equalsigns(int x, int y);
_Bool isEven(int x);
