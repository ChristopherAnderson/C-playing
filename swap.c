#include <stdio.h>
#include <malloc.h>
#include "utility_fns.h"

// moved swap() to utility

int main(void) {
  // method one: sending address-of some ints  (&var)
  int first=4, second=32;
  printf("first value: %i\nsecond value: %i\n", first, second);
  swap(&first, &second);
  printf("first value: %i\nsecond value: %i\n", first, second);

  // method two: allocating mem, sending pointers
  int *one=malloc(sizeof(int)), *two=malloc(sizeof(int));
  *one = 1; *two = 2;
  printf("one value: %i\ntwo value: %i\n", *one, *two);
  swap(one, two);
  printf("one value: %i\ntwo value: %i\n", *one, *two);

  free(one); free(two);

  // testing the bitswap
  printf("first value: %i\nsecond value: %i\n", first, second);
  bitswap(&first, &second);
  printf("first value: %i\nsecond value: %i\n", first, second);

  return 0;
}
