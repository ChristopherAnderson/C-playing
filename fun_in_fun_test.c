// compiles just fine
//
//
// CORRECTION: (also fascinating)
// Standard C does not allow functions to be defined inside of functions
// this will compile due to an extension on gcc
//
// Compile with -pedantic for the warning: error: ISO C forbids nested functions
// Compile with -pedantic -Werror to make it an error
#include <stdio.h>

int main()
{
  int three(void)
  { return 3; }

  return 3 + three();
}
