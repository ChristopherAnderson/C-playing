#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void astrncpy(char **base, char *copyme) {
  int length = strlen(copyme);
//  *base = realloc(*base, sizeof(char)*length+1);
  // sizeof(char) always = 1
  // Must be length+1 because strlen does not count '\0'
  *base = realloc(*base, length+1);
  strncpy(*base, copyme, length+1);
}

int main(void) {
  char str[] = "hello"; 
  // must be initialized to NULL
  // If ptr passed to realloc isn't null, realloc 
  // assumes it's a valid heap pointer. When realloc 
  // tries to use that pointer, it triggers a segfault 
  // because it does not refer to any valid memory.
  char *anotherstr = NULL;

  // copy contents of str into anotherstr
  // str is a pointer to char
  // &anotherstr is the address of a ptr to char
  // a pointer to a pointer to char
  astrncpy(&anotherstr, str);
  printf("%s\n", anotherstr);

  return 0;
}
