#include <stdlib.h>
#include <stdio.h>
#define MAX 16

int main() {
  char name[MAX];
  int a[10];
  a[10]=1;   // oh no!

  printf("Hello World....\n");
  printf("You are: %s\n", getenv("USER"));
  printf("Who would you like to be? ");
  // fgets(name, MAX, stdin);
  scanf("%s", name);
  setenv("USER", name, 1);
  printf("Now you are %s, but that's okay", getenv("USER"));
  printf(" because we can't overwrite the variables in the");
  printf(" environment of the parent process (the shell)\n");
}
