// implicit declaration
// only describes, assumes it's allocated elsewhere
// it's allocated in extern_example1.c
extern int Global_var;

// Function header (definition)
void SomeFunction(void)
{
  ++Global_var;
}

