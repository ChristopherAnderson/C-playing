CC=gcc
CFLAGS=-I. -Wall -Wextra -g
ASAN_FLAGS = -fsanitize=address

hellomake: helloworld.o 
	$(CC) $(CFLAGS) $(ASAN_FLAGS) -o helloworld helloworld.c

factorial: factorial.c
	$(CC) $(CFLAGS) -o factorial factorial.c

.PHONY : clean
clean: 
	rm -f helloworld factorial *.o

