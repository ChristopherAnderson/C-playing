#include <stdio.h>

int main(void) {
  int x = 5;
  int y = 2;

  printf("x is %d \n", x);
  printf("y is %d \n", y);
  printf("x + y is %d \n", x + y);
  printf("x - y is %d \n", x - y);
  printf("x * y is %d \n", x * y);
  printf("x / y is %.1f \n", x / (float) y);
  
  return 0;
}
