// a use of the do {} while (); 
// want to iteratively evaluate a function until it converges

int main()
{
  float evaluate_function();
  float error;

  float epsilon = 1e-3;
  do {
    error = evaluate_function();
  } while (error > epsilon);

  return 0;
}
