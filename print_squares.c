#include <stdio.h>

void print_array(int in_array[], int array_size)
{
  for (int i=0; i<=array_size; i++)
  {
    printf("Number:\tSquare:\n"
           "     %i\t%i\n", i, i*i);
  }
}
int main()
{
  int a[] = {1,2,3,4};
  print_array(a, 4);
  return 0;
}
