// gcc links against stdio automatically
// can skip #include <stdio.h> by writing the prototype
// -- not really helpful but demonstrative
int puts(const char *str);

int main() {
    puts("hello world!");
    return 0;
}
