#include <stdio.h>
#include <stdlib.h> // rand()
#include <time.h>   // time()

int main()
{
  srand(time(NULL)); // random seed

  int size = 10;
  int data[size];

  // create the random data
  for (int i=0; i<=size-1; i++)
  {
    int r = rand() % 20;
    data[i] = r;
    printf("#%i: %i\n", i, r);
  }

  return 0;
}
