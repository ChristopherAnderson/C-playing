// modification of factorial3 to use &a instead of *a 
#include <stdio.h>
#include <malloc.h> 

int globe=1;

int factorial(int *a_copy) { 
  while (*a_copy) {         
    globe *= *a_copy;
    (*a_copy)--;
  }
  return globe;
}

int main(void) {
  int a = 10;
  printf("%i factorial", a);
  printf(" is %i.\n", factorial(&a));
  printf("a is %i, globe is %i\n", a, globe);

  return 0;
}
