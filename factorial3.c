// revisiting factorial, using pointers (call by address)
#include <stdio.h>
#include <malloc.h>  // can use either
// #include <stdlib.h>

int globe=1;

int factorial(int *a_copy) { // frame has a copy of the pointer
  while (*a_copy) {          // but they point to the same thing
    globe *= *a_copy;
    (*a_copy)--;
  }
  return globe;
}

int main(void) {
  int *a = malloc(sizeof(int));
  *a = 10;
  // Weird!!! this line prints *a as 0, which is what it should be
  // AFTER the factorial(a) frame has been destroyed.
//  printf("%i factorial is %i\n", *a, factorial(a));
  printf("%i factorial", *a);
  printf(" is %i.\n", factorial(a));
  printf("*a is %i, globe is %i\n", *a, globe);
  free(a);

  return 0;
}
