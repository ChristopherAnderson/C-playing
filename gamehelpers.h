typedef struct exit {
  int connects;
  int direction;
} exit;

typedef struct room { 
    exit *exits;
} room;

typedef struct player {
  int id;
  int group;
  int level;
  int points;
  int currentroom;
} player;

void openclose(room *, int);
int exitcount(int);
player addplayer(int*, int**);
void status(void); // take arguments later