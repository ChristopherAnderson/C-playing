#include <math.h>
#include <stdio.h>

int main()
{
  double no_match = 1;
  double matches_me;
  int count;

  printf("People\t Matches me\t Any match\n");
  for (count=2; count<=40; count++)
  {
    // calculating the complement -- that nobody shares a bday
    // odds that one person does not share the 1st person's bday is 364/365.
    // Odds that two people don't share the 1st person's bday is (364/365)^2,...
    matches_me = 1 - pow(364/365., count-1);
    // odds that 2nd person doesn't share 1st person's bday is (364/365, odds
    // that additional person shares no bday with first two given that the first
    // two don't share a bday is 363/365.
    // So odds that first three don't share a bday is (364/365)(363/365)
    no_match *= (1 - (count-1) / 365.);
    printf("%i\t %.3f\t\t %.3f\n", count, matches_me, (1-no_match));
  }

  return 0;
}

