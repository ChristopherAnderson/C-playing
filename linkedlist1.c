// C Linked List Demonstration, from
// http://www.zentut.com/c-tutorial/c-linked-list/
#include <stdio.h>
#include <stdlib.h> // exit()

typedef struct node {
  int data;
  struct node *next;
} node;

node *create(int data, node *next) {
  // why need to cast to node*?
  node *new_node = (node*)malloc(sizeof(node));
  if (new_node == NULL) {
    printf("Error creating new node\n");
    exit(1);
  }
  new_node->data = data;
  new_node->next = next;

  return new_node;
}

node *prepend(node *head, int data) {
  node *new_node = create(data, head);
  head = new_node;
  
  return head;
}

// a function pointer that points to a function for
// node manipulation
// a callback function
typedef void (*callback)(node *data);

// to traverse the linked list, start from first node
// and move to next until reach a NULL pointer
void traverse(node *head, callback f) {
  node *cursor = head;
  while (cursor != NULL) {// !cursor ?
    f(cursor);
    cursor = cursor->next;
  }
}

// same traversing technique to count number elems
int count(node *head) {
  node *cursor = head;
  int c = 0;
  while (cursor != NULL) {
    c++;
    cursor = cursor->next;
  }
  return c;
}

// add a new node at the end
// first find the last node
node *append(node *head, int data) {
  // go to last node
  node *cursor = head;
  while (cursor->next != NULL) { 
    cursor = cursor->next; 
  }

  // create new node
  node *new_node = create(data, NULL);
  cursor->next = new_node;

  return head;
}

// insert new node after particular node
// verify it exists, call it "prev"
// point the next pointer of the new node to the next
// node that the next pointer of the prev node points to
// point the next pointer of the "prev" node to the new node
node *insert_after(node *head, int data, node *prev) {
  node *cursor = head;
  while (cursor != prev) {
    cursor = cursor->next; 
  }

  if (cursor != NULL) {
    node *new_node = create(data, cursor->next);
    cursor->next = new_node;
    return head;
  } else {
    return NULL;
  }
}

// insert a new node before a particular node
// if the nxt node is the first node, can call prepend()
// otherwise find the previous node of the nxt node, call it cursor
// Then, point the next pointer of new node to the node that the
// next pointer of the cursor points to, and point the next pointer
// of the cursor to the new node
node *insert_before(node *head, int data, node *nxt) {
  if (nxt == NULL || head == NULL) {
    return NULL;
  }

  if (head == nxt) {
    head = prepend(head, data);
    return head;
  }

  // find the prev node, starting from first
  node *cursor = head;
  while (cursor != NULL) {
    if (cursor->next == nxt) {
      break;
    }
    cursor = cursor->next;
  }

  if (cursor != NULL) {
    node *new_node = create(data, cursor->next);
    cursor->next = new_node;
    return head;
  } else {
    return NULL;
  }
}

// search for node
// scan whole list and return first node that stores searched data
// TODO: alter to find all instead of first
// returns NULL if no node stores input data
node *search(node *head, int data) {
  node *cursor = head;
  while (cursor != NULL) {
    if (cursor->data == data) {
      return cursor;
    }
    cursor = cursor->next;
  }
  return NULL;
}

// sort using Insertion Sort
node *insertion_sort(node *head) {
  node *x, *y, *e;
  x = head;
  head = NULL;

  while (x != NULL) {
    e = x;
    x = x->next;
    if (head != NULL) {
      if (e->data > head->data) {
        y = head;
        while ((y->next != NULL) && (e->data > y->next->data)) {
          y = y->next;
        }
        e->next = y->next;
        y->next = e;
      }
      else {
        e->next = head;
        head = e;
      }
    }
    else {
      e->next = NULL;
      head = e;
    }
  }
  return head;
}

// reverse linked list
// change the next pointer of each node from the next node
// to the previous node
node *reverse(node *head) {
  node *prev = NULL;
  node *current = head;
  node *next;
  while (current != NULL) {
    next = current->next;
    current->next = prev;
    prev = current;
    current = next;
  }
  head = prev;
  return head;
}

// delete a node from front of list
// point the head to the next node, and remove the node head pointed to
// if list only has one node, set head pointer to NULL
node *remove_front(node *head) {
  if (head == NULL)
    return NULL;
  node *front = head;
  head = head->next;
  front->next = NULL;
  // if this is last node in list
  if (front == head)
    head == NULL;
  free(front);
  return head;
}

// delete node from back of list
// use two ptrs, cursor and back, to track the node
// start from first node until cursor pointer reaches last node
// and the back pointer reaches the node before the last node
// Set the next pointer of the back to NULL, delete node cursor points to
// If node has only 1 elem, set head to NULL before removing node
node *remove_back(node *head) {
  if (head == NULL)
    return NULL;

  node *cursor = head;
  node *back = NULL;
  while (cursor->next != NULL) {
    back = cursor;
    cursor = cursor->next;
  }
  if (back != NULL)
    back->next = NULL;

  // if this is last node in list
  if (cursor == head)
    head = NULL;

  free(cursor);
  return head;
}

// delete node in middle of linked list
// If node is first node: remove_front()
// If node is last node, remove_back()
// otherwise traverse from first node, use cursor pointer to point to
// the node before the node that needs removing
// Use a tmp pointer to point to the node that needs removed
// set next pointer of cursor point to the node that the next pointer
// of tmp points to
// Remove node that tmp pointer points to
node *remove_any(node *head, node *nd) {
  // if node is first node
  if (nd == head) {
    head = remove_front(head);
    return head;
  }
  // if node is last node
  if (nd->next == NULL) {
    head = remove_back(head);
    return head;
  }
  // if node is in middle
  node *cursor = head;
  while (cursor != NULL) {
    if (cursor->next = nd)
      break;
    cursor = cursor->next;
  }

  if (cursor != NULL) {
    node *tmp = cursor->next;
    cursor->next = tmp->next;
    tmp->next = NULL;
    free(tmp);
  }
  return head;
}

// delete whole linked list
void dispose(node *head) {
  node *cursor, *tmp;

  if (head != NULL) {
    cursor = head->next;
    head->next = NULL;
    while (cursor != NULL) {
      tmp = cursor->next;
      free(cursor);
      cursor = tmp;
    }
  }
}

void display(node *n) {
  if (n != NULL)
    printf("%d ", n->data);
}

void menu()
{
    printf("--- C Linked List Demonstration --- \n\n");
    printf("0.menu\n");
    printf("1.prepend an element\n");
    printf("2.append an element\n");
    printf("3.search for an element\n");
    printf("4.insert after an element\n");
    printf("5.insert before an element\n");
    printf("6.remove front node\n");
    printf("7.remove back node\n");
    printf("8.remove any node\n");
    printf("9.sort the list\n");
    printf("10.Reverse the linked list\n");
    printf("-1.quit\n");
 
}

int main(void) {
  int command = 0;
  int data;
  node *head = NULL;
  node *tmp = NULL;
  callback disp = display;

  menu();
  while(1) {
    printf("\nEnter (0-10, -1 to quit): ");
    scanf("%d", &command);
    if (command == -1)
      break;
    switch(command)
        {
        case 0:
            menu();
            break;
        case 1:
            printf("Please enter a number to prepend:");
            scanf("%d",&data);
            head = prepend(head,data);
            traverse(head,disp);
            break;
        case 2:
            printf("Please enter a number to append:");
            scanf("%d",&data);
            head = append(head,data);
            traverse(head,disp);
            break;
        case  3:
            printf("Please enter a number to search:");
            scanf("%d",&data);
            tmp = search(head,data);
            if(tmp != NULL)
            {
                printf("Element with value %d found.",data);
            }
            else
            {
                printf("Element with value %d not found.",data);
            }
            break;
        case 4:
            printf("Enter the element value where you want to insert after:");
            scanf("%d",&data);
            tmp = search(head,data);
            if(tmp != NULL)
            {
                printf("Enter the element value to insert after:");
                scanf("%d",&data);
                head = insert_after(head,data,tmp);
                if(head != NULL)
                    traverse(head,disp);
            }
            else
            {
                printf("Element with value %d not found.",data);
            }
            break;
        case 5:
            printf("Enter the element value where you want to insert before:");
            scanf("%d",&data);
            tmp = search(head,data);
            if(tmp != NULL)
            {
                printf("Enter the element value to insert before:");
                scanf("%d",&data);
                head = insert_before(head,data,tmp);
 
                if(head != NULL)
                    traverse(head,disp);
            }
            else
            {
                printf("Element with value %d not found.",data);
            }
            break;
        case 6:
            head = remove_front(head);
            if(head != NULL)
                traverse(head,disp);
            break;
        case 7:
            head = remove_back(head);
            if(head != NULL)
                traverse(head,disp);
            break;
        case 8:
            printf("Enter the element value to remove:");
            scanf("%d",&data);
            tmp = search(head,data);
            if(tmp != NULL)
            {
                remove_any(head,tmp);
                if(head != NULL)
                    traverse(head,disp);
            }
            else
            {
                printf("Element with value %d not found.",data);
            }
            break;
        case 9:
            head = insertion_sort(head);
            if(head != NULL)
                traverse(head,disp);
            break;
        case 10:
            head = reverse(head);
            if(head != NULL)
                traverse(head,disp);
            break;
        }
 
    }
    dispose(head);
    return 0;
}
