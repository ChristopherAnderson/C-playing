#include <stdio.h>
#define QUESTION_COUNT (3)

void question_wouldLike(int);

void evaluate(int answer, int actual)
{
  if (answer == actual)
  {
    printf("Correct\n");
  } else {
    printf("Sorry, the answer is %d\n", actual);
  }
  return;
}


int main()
{
  int answer, actual, count = QUESTION_COUNT;
  
  while(count)  
  {
    printf("sizeof(char) returns: ");
    actual = sizeof(char);
    scanf("%d", &answer);
    evaluate(answer, actual);
    count--;
  }

  return 0;
}


void question_wouldLike(int count)
{
  const char *questions[count];   // why const? 
  questions[0] = "sizeof(char) returns: ";
  questions[1] = "sizeof(int) returns: ";
  questions[2] = "sizeof(double) returns: ";

  return;
}
