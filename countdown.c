#include <stdio.h>

void method1(int n, int *array)
{
  for (; n > 0; --n)
  { printf("%i\n", array[n-1]); }
}

void method2(int n, int *array)
{
  for (; n > 0; )
  { printf("%i\n", array[--n]); }
}

int main()
{
  int n = 3;
  int a[] = {6,7,8};

  method1(n, a);
  method2(n, a);

  return 0;
}

