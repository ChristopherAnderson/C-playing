#include <stdio.h>

void print_array(int in_array[], int array_size) {
  for (int i=0; i<=array_size; i++) {
    printf("Number:\tSquare:\n"
           "       %i\t    %i\n", i, i*i);
  }
}

void swap(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}
// no intermediate value
void bitswap(int *a, int *b) {
  *a = *a ^ *b; // *a is XOR of the two
  *b = *a ^ *b; // *b is the original *a
  *a = *a ^ *b; // *a is the original *b
}

// char[] binaryString(int num) {
//   int k, i, size = digitcount(num);
//   for (i=size-1; i>=0; i--) {
//     k = num >> i;
// 
// }

int bitmax(int x, int y) 
{ return x ^ ((x^y) & -(x<y)); }

// look up CHAR_BIT, __CHAR_BIT__, __builtin_clz()
int digitcount(int N)
{ return (sizeof(N)*__CHAR_BIT__)-__builtin_clz(N); }

int bitabs(int x)
{ return (x ^ (x >> 31)) - (x >> 31); }

_Bool equalsigns(int x, int y)
{ return (x ^ y) >= 0; }

_Bool isEven(int x) 
{ return (x & 1) == 0; }

// divisor must be a power of 2
int bitmod(int num, int div)
{ return num & (div-1); }

int bitflipsign(int x)
{ return ~x+1; }
