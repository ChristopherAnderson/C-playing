// error: too many arguments to function 'fun2'
// yet it doesn't mind fun1 --
// that's because fun1 is not a function that accepts no arguments,
// it's a function that accepts any arguments
void fun1() {}

void fun2(void) {}

int main() {
  fun1();
  fun1(3);

  fun2();
  fun2(4);

  return 0;
}
