#include <stdio.h>

int main()
{
  int fibo[20];
  fibo[0] = 0;
  fibo[1] = 1;

  for (int i=2; i<20; i++)
  {
    fibo[i] = fibo[i-1] + fibo[i-2];
    printf("i:\tn:\tn-1:\tn-2:\tn/(n-1):\n"
           "%d\t%d\t%d\t%d\t%g\n\n", i, fibo[i], fibo[i-1], fibo[i-2],
                                    (double)fibo[i]/fibo[i-1]);
  }

  return 0;
}
