// useful for commenting out embedded comments

void aFunction(int);

int main()
{
  int a,b,c;

#if 0
  aFunction(a);
  aFunction(b);

  /* A comment */
  for (int i=0; i<10; i++)
  {
    aFunction(c);
  }

#endif

  return 0;
}
