#include <stdio.h>

int main(void) { 
  typedef char fourletterword[4];
  fourletterword okay = "okay";
  // fourletterword house = "house"; // error

  // a regular struct
  struct s1 {
    int a;
    int b;
  };

  // an anonymous struct given typedef of s2
  typedef struct {
    int x;
    int y;
  } s2;

  // examples of the above two, they work the same
  struct s1 location = { 3, 4 };
  struct s1 place = { .a = 5, .b = 6 };
  s2 point = { 2, 2 };
  s2 origin = { .x = 0, .y = 0 };
  printf("location.a: %i, place.b: %i, point.x: %i, origin.y: %i\n",
          location.a, place.b, point.x, origin.y);


  // a struct called hello, given a typedef hiStructTypedef
  typedef struct hello {
    char hi;
    short int number;
  } hiStructTypedef;

  // a struct called audience, with a variable called convention created
  struct audience {
    int headcount;
    char* location;
  } convention;

  // now we will have two variables of type "struct audience"
  // struct audience convention, struct audience concert
  struct audience concert;

  typedef int four_things[4];
  typedef struct {
    four_things sides;
    double area;
  } rectangle;

  rectangle known_area = {.area = 25 };
  rectangle three_sides = { .sides = {[1 ... 3] = 5}};
  printf("Rectangle known area: %.2f, unknown sides:\n", known_area.area);
  for (int i=0; i<4; i++) {
    printf("side%i: %i, ", i+1, known_area.sides[i]);
  }
  printf("\n");

  printf("Rectangle with three known sides,\n");
  for (int i=0; i<4; i++) {
    printf("side%i: %i, ", i+1, three_sides.sides[i]);
  }
  printf("\n");
  
  
  return 0;
}
