#include <stdio.h>

struct {
  unsigned int flag1;
  unsigned int flag2;
} struct1;

// variables defined with a set width are  bit fields
// type membername : width;
struct {
  unsigned int flag1 : 1;
  unsigned int flag2 : 1;
  unsigned int expire : 3; // can hold values 0-7
} struct2;


int main()
{
  printf("sizeof(struct1): %li\n", sizeof(struct1)); // 8
  printf("sizeof(struct2): %li\n", sizeof(struct2)); // 4

  // trying to use more than our 3 bits in struct2.expire
//  struct2.expire = 9;
//  Gives a warning: large integer implicitly truncated to unsigned type
//  [-Woverflow]
  printf("struct2.expire: %d\n", struct2.expire);
  // prints 1
  return 0;
}
