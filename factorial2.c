// *Copies* of variables are put into the frame of the function call
//    (when they are in the function's argument list)
//
// but variables that are *global* (to the definition of the function)
//    are put in all function frames

#include <stdio.h>

int global = 1;

double factorial(int number_copy) {
  while(number_copy) {
    global *= number_copy;
    number_copy--;
  }
  return global;
}

int main() {
  int num = 10;
  printf("%i! = %f.\n", num, factorial(num));
  
  
  return 0;
}
