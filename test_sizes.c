#include <stdio.h>

int main()
{
  char *datatypes[5] = {"int", "float", "long", "double", "char"};
  
  for (int i=0; i<5; ++i)
  {  
    printf("%s: %ld\n", datatypes[i], sizeof(datatypes[i]));
  }
}
