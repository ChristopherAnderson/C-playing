#include <stdio.h>
#include "utility_fns.h"

void binary(int num) {
  int k, size = digitcount(num);

  printf("%i in binary is: ", num);
  for (int i=size-1; i>=0; i--) {
    k = num >> i;

    if (k & 1) {
      printf("1");
    } 
    else {
      printf("0");
    }
  }
  printf(" and it has %i digits\n", size);
}

int main() {
  
  for(int i=0; i<34; i++) {
    binary(i);   
  }


  return 0;
}
