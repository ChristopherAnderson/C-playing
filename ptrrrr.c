// demonstrating const with pointers

#include <stdio.h>

int main(void)
{
  int i = 10;
  int j = 20;
  int *ptr = &i;  // ptr is a pointer to an int, specifically i
  printf("*ptr: %d\n", *ptr);

  // changing the variable that ptr is pointing to
  ptr = &j;
  printf("*ptr: %d\n", *ptr);

  // changing the value that is stored by pointer
  *ptr = 100;
  printf("*ptr: %d\n", *ptr);


  /////////// introducing const

  // two ways
  const int *ptr2;
  int const *ptr3;  // are they different
  
  // can change pointer to point at other variable
  // but can't change the value pointed 
  // thing says "pointer is stored in r-w area (stack in this case)
  // object pointed may be in ro or r-w area"
  int a = 10;
  int b = 20;
  const int *ptr4 = &a;   // ptr is pointer to constant


  return 0;
}
