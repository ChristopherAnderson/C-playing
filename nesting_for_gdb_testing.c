#include <stdio.h>
#define NAME "Leonard"

int fun1(char c, int n) {
  int counter=n;
  static int length=0;
  while(counter) {
    printf("%c", c);
    counter--;
  }
  printf("\n");
  
  length++;      // counting how many times fun1 has been called
  return length; // ie how many letters in the name
}

int fun2(char name[]) {
  int fun1count, i=0;
  while (*(name+i)) { // proceed until '\0'
    fun1count = fun1(*(name+i), 3); // pass in each letter
    i++; // increment the offset in the char array
  }
  printf("Called fun1 %i times\n", fun1count);

  return 0;
}
  

int main() {
  char *name = NAME;
  int j=0;
  while (*(name+j)) {
    printf("%c ", *(name+j));

    j++;
  }
  printf("\n");
  fun2(name);
  return 0;
}
