#include <stdio.h>

// takes an index and returns the ith triangular number
//   ie triangular(6) returns 1+2+3+4+5+6=21
int triangular(int i) {
  int trinum = 0;
  for (i; i>0; i--) {
    trinum += i;
  }
  return trinum;
}

// returns the index of the smallest triangular number larger than input
//   ie next_triangular(12) returns the index 5, 
//   because triangular(5) = 15 (>input)  and triangular(4) = 10 (<input)
int next_triangular(int input) {
  int index = 0;
  while (triangular(index) <= input) {
    index++;
  }
  return index;
}

// out[0] + out[1] + out[2] = in, where each out[i] is triangular
void find_triplet(int in, int out[]) {
  int i,j,k;
  int maxindex = next_triangular(in);
  for (i=0; i < maxindex; i++) {
    for(j=0; j < maxindex; j++) {
      for(k=0; k < maxindex; k++) {
        if (triangular(i)+triangular(j)+triangular(k) == in) {
          out[0] = triangular(i);
          out[1] = triangular(j); 
          out[2] = triangular(k);
          printf("%i + %i + %i = %i\n", out[0], out[1], out[2], in);
          return;
        }
      }
    }
  }
  // this should never print
  printf("none found for %i\n", in);
}

int main() {
  int i, out[3], n=100;

  for (i=1; i<=n; i++) {
    find_triplet(i, out);
  }   
  return 0;

}
