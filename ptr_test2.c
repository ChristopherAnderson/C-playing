#include <stdio.h>
#include <stdlib.h> // calloc

int main() {
  double balance[5] = {1000.0, 2.0, 3.4, 16.3, 100.2};
  double *p = balance;
  int i, people=5; 
  int *data = calloc(people, sizeof(int)); // alloc and set to 0
  data[2] = 4;
  for (i=0; i<people; i++) {
    printf("data[%i] is: %i\n", i, data[i]);
  }

  printf("Array values using pointer\n");
  for (i=0; i < 5; i++) {
    printf("*(p + %d): %f\n", i, *(p + i));
  }

  printf("Array values using balance as address\n");
  for (i=0; i<5; i++) {
    printf("*(balance + %d): %f\n", i, *(balance + i));
  }
  
  free(data);
  return 0;
}

