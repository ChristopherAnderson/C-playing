#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void astrncat(char **copyonto, char *copyme) {
  int length = strlen(copyme) + strlen(*copyonto);
  *copyonto = realloc(*copyonto, length+1);
  strncat(*copyonto, copyme, length+1);
}

int main(void) {
  char *s1 = "hi";
  char *s2 = " there";
  astrncat(&s1, s2);
  printf("%s\n", s1);

  return 0;
}
