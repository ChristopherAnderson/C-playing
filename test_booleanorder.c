// demonstrates higher precedence of && over ||

#include <stdio.h>

int main()
{
  if (1 || 0 && 0)        // (1 || (0 && 0) == (1 || 0) == 1
  { printf("hello\n"); }  // does print this
  else
  { printf("Nope\n"); } // ((1 || 0) && 0) == (1 && 0) == 0

  return 0;
}

