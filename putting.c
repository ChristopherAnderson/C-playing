#include <stdio.h>

int main()
{ 
  // stdout 
  putchar('O');
  putchar('h');
  putchar(' ');
  puts("Hi");
 
  // file
  FILE *save = fopen("savegame.txt", "w");
  fputs("save game\n", save);
  
  char status1 = 'A';
  char status2 = 'B';
  fputc(status1, save); // A%
  fputc(status2, save); // AB%
  fputc('\n', save);    // "\n" is an Error, '\n' correct

  return 0;

}
