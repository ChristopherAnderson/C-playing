#include <stdio.h>

int main()
{
  int var[] = {10, 100, 200};
  int *ptr = var;
  printf("%d\n", *ptr);
  printf("%d\n", *(++ptr));
}
