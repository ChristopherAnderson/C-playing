#include <stdio.h>
#include <string.h>
#define STRING_COUNT 3
#define STRING_LENGTH 4

int main() {
  
  // these two strings are the same
  char stringA[] = "aaaa";
  char stringB[] = {'a', 'a', 'a', 'a', '\0'};
  
  // following example from SO
  char *string2 = "okok";
  // what happens, what compiler does, is as if:
  static char __temporaryarray[] = "okok";
  char *string2new = __temporaryarray;

  // and since string2 is a ptr to char, ie what it points to is chars, not
  // arrays of chars, it's really:
  char *string2old = &__temporaryarray[0];

  // all strings same (max) length
  // declare a 2d array of char
  char strs[STRING_COUNT][STRING_LENGTH+1];
  strcpy(strs[0], "hi!!");
  char a[] = "oooo";
  strcpy(strs[1], a);
  
  

  return 0;
}
