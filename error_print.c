#include <stdio.h>

void error_print(FILE *ef, int error_code, char *msg)
{
  fprintf(ef, "Error %i occured: %s.\n", error_code, msg);
}

int main()
{
  FILE *error_file = fopen("example_error_file.txt", "w"); // open for writing
  error_print(error_file, 37, "No! Bad!");
}
