// logic in toggler() is screwy
#include <stdio.h>

int array[] = {23, 5, 45};
int *ptr_array = array;

int toggler(int *array_pt) {
  static int index = 0;
  int value = array_pt[index];
  if (index == 2) {
    printf("resetting index\n");
    index = 0;
  }
  else 
    index++;

  return value;
}

int main(void) {
  for (int i = 0; i<10; i++) {
    switch (toggler(ptr_array)) {
      case 23:
        printf("%i\n", 23);
        break;
      case 5:
        printf("%i\n", 5);
        break;
      case 45:
        printf("%i\n", 45);
        break;
      default:
        printf("oops\n");
    }
  }

  return 0;
}
