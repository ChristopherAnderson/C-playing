#include <stdio.h>
#include <stdlib.h>
#include "utility_fns.h"

int main(void) { 
  int i, length = 50;
  int *squares = malloc(length * sizeof(int));
  // compute squares
  for (i=0; i<length; i++) {
    squares[i] = i*i;
  }
  // swap two of them
  swap(&squares[3],&squares[8]); 

  for (i=0; i<length; i++) {
    printf("i: %i\tsquares[%i]: %i\n", i, i, squares[i]);
  }

  return 0;
}

