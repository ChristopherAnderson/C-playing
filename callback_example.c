// from SO
// note: 
// "The dereference operator is optional with function pointers, 
// as is the addressof operator. 
// myfunc(...) = (*myfunc)(...) and &myfunc = myfunc "
#include <stdio.h>
#include <stdlib.h> // rand()

// size_t: unsigned data type, def'd in stddef.h
// represents size of an object
// "guaranteed to hold any array index"
void populate_array(int *array, size_t arraySize, int (*blahdyblah)(void)) {
  for (size_t i=0; i<arraySize; i++)
    array[i] = blahdyblah(); // getNextRandomValue() was passed in
}

int getNextRandomValue(void) {
  return rand() % 20;
}

// another callback
void ListExits(int (*whichroom)(void)) {
  printf("Exits: %i\n", whichroom());
}

int room1(void) {
  return 6;  // 0110
}

int room2(void) {
  return 9;  // 1001
}

int main(void) {
  int myarray[10];
  populate_array(myarray, 10, getNextRandomValue);

  for (int i=0;i<10;i++)
    printf("%d ", myarray[i]);
  printf("\n");
  ListExits(&room1);
  ListExits(&room2);
  return 0;
}
