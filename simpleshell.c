#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv) {
  if (argc == 1) {
    printf("Give me a command to run.\n");
    return 1;
  }
  // say argv[1] is "ls"
  // system() puts the ls program's main() on top of the current stack
  // on completion, that frame is destroyed, and ls has returned a value
  int return_value = system(argv[1]);
  printf("The program returned: %i\n", return_value);
 
  return return_value;
}
