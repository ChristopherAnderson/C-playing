// needs fixing and playing around
// the thought is returning a static int from main
// or trying to think of a use-case for that anyhow
#include <stdio.h>

void countdown(void)
{
  static int count = 7;
  count--;
}

int main()
{
  while(count)
    countdown();

  return count;
}
